// s27 a1 for WDC028-27 Node.js Routing w/ HTTP Methods

let http = require("http");

http.createServer(function(request, response) {

	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}

	// GET Request to retrieve information
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Welcome to your profile!`);
	}

	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Here's our courses available`);
	}

	// POST request to create/add information
	if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Add a course to our resources`);
	}

	// PUT
	if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Update a course to our resources`);
	}

	// DELETE
	if(request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Archive courses to our resources`);
	}


}).listen(4000);

console.log(`Server now accessible at localhost:4000`);

